﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class ColumnsPoolModif : MonoBehaviour {
    [System.Serializable]
    public class DifficultyConfig {
        public int Score;
        public SpawnConfig Config;
        
    }

	public int columnPoolSize =5;
	// on va creer 5 colonnes
	public GameObject columnPrefab;

    public List<DifficultyConfig> DifficultyLevels;


	private GameObject[] columns;
	private int currentColumn =0;

	[System.Serializable]

	public class SpawnConfig
	{
		
		/// The scroll speed of the game.
		[Range (0.5f, 2.5f)]

		public float ScrollSpeed = 1;

		/// Spawn position X of the first column.

		public float StartX = 5;

		/// The min spawn poss X of the next column.

		public float SpawnMin=4;
	
		/// The max spawn poss X of the next column.

		public float SpawnMax=7;
	
		/// Min spawn poss y of the next column .
	
		public float HMin=-1;

		// Max spawn poss Y of the next column.

		public float HMax=3.5f;

		// Min spacing between the top and the bottom colum.

		public float SpacingMin=2;

		// Max spacing between the top and the bottom colum.

		public float SpacingMax=3.5f;
	
		/// Chance of the colum to have a top and bottom part 
		[Range(0,1)]

		public float PrctDouble =0.5f;

	}




	// Use this for initialization
	void Start ()
	// creer un tableau de GameObject de taille ColumPoolSize
	{
		
		columns = new GameObject[columnPoolSize];
		// pour chaque case du tableau 

		for (int i = 0; i < columnPoolSize; i++)
		{
			// creer un objet colonne
			// stocke une reference vers l objet dans le tableau 

			columns [i] = Instantiate (columnPrefab); 
			// desactiver l'objet 

			columns [i].SetActive (false);


		}
        SpawnColumn(DifficultyLevels[0].Config, 0);
	}

	// Spawn a column in the game 

	// < param name ="config">Spawn configuration </ param>
	// pour faire une spawn de colonne on a besoin de : la postion de la dern colone spawner )
	// declaration de la fonction :
	void SpawnColumn (SpawnConfig config , float lastColumnPositionX) {
		// Case du tableau qui contient la colonne et a activer 
		
		//Position de la derniere colonne en x


		// ensuite on spawn , on vas recuperer la derniere colonne 
		// current column est la derniere colonne spawn 
		// Case du tableau qui contient la colonne à positionner et à activer 
		var nextColumn = currentColumn +1 >= columnPoolSize ? 0 : currentColumn +1;
		//on recupere la colonne
		var column = columns [nextColumn];
		//on l active
		column.SetActive (true);
		Vector3 position = new Vector3(lastColumnPositionX, 0, 0);
			// positionement en X (spawn min / spawn max)
		// y =0 doit etre au sol ds un jeu video , ici non donc cela complique , pensez a cela dans l avanir
		// donc on a recup la position de la derniere colonne 
		//Poitionnement en X (SpawnMin et SpawnMAx)
		position.x +=Random.Range(config.SpawnMin, config.SpawnMax);
		//Positionnement en Y( HMin HMax)
		position.y += Random.Range (config.HMin, config.HMax);
		column.transform.position = position;

		// est une colonne avec deux partie ?
		bool IsDouble = Random.Range (0f, 1f) < config.PrctDouble;
		// réactiver tout les enfants 
		foreach (Transform child in column.transform){
			child.gameObject.SetActive (true);
		}
		// si cela n est pas une colone a deux parties


		if (!IsDouble)
		{
			// l'enfant à faire disparaitre 
			var childCount = column.transform.childCount;
			var hidePart = column.transform.GetChild (Random.Range (0, childCount));
			//cacher l enfant 
			hidePart.gameObject.SetActive(false);

			// maintenant il nous reste a gerer l espacement
			//calcule de l espacement

		}

		// espacement entre les colonnes

		var spacing = Random.Range(config.SpacingMin , config.SpacingMax);
		column.transform.GetChild (0).localPosition = Vector3.down * spacing * 0.5f;
		column.transform.GetChild (1).localPosition= Vector3.up * spacing * 0.5f;

		// Mise a jour de la colonne actuelle 
		currentColumn = nextColumn;
		// il nous reste a spawner les colonnes
		// on vas chercher les positionnement 
		// on se base sur l ecran 16.9 , et on regarde la position au debut de lecran ...8 et qq , donc des qu elle est en dessus de 8 ( la derniere spawn ) en x , il faut que je fasse en sorte que je spawn la collonne suivante 


	}


	private void Update ()
	{
		if (columns [currentColumn].transform.position.x <= 8)
            // algo pr trouver la difficulté 
		{
            var difficulty = DifficultyLevels
                // on ordonne de maniere decroissante en fonction du score
                .OrderByDescending(c => c.Score)
                // on choisi le premier niveau de difficulté avec un scre plus petit que le notre
                .FirstOrDefault(c => c.Score <= GameControl.instance.Score);

            // on utulise la configuration du niveau de difficulté pour creer le prochain pilier

			SpawnColumn (difficulty.Config,columns[currentColumn].transform.position.x);
		}
	}
}
